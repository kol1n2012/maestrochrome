<?
  function resize_img($fname,$dname,$x,$y) {
    $old = imageCreateFromJpeg($fname);
    if ($old===false)
      $old = imageCreateFromGif($fname);
    if ($old===false)
      $old = imageCreateFromPng($fname);
    if ($old===false) return false;

    $w = imageSX($old);
    $h = imageSY($old);

    $s1=$w/$h;
    $y=round($x/$s1);

    $new = imageCreateTrueColor($x, $y);
 
    imageCopyResampled($new, $old, 0, 0, 0, 0, $x, $y, $w, $h);
    imageDestroy($old);

    imageJpeg($new, $dname);
    imageDestroy($new);

    return true;
  }
?>