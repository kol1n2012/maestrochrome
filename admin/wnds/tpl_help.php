<html><head>
<link href="../style.css" type=text/css rel=stylesheet>
</head><body leftmargin=15>
<font class=title>�������</font><br><br>
<p>� ����� ������� ��� ��������� ������� ����� ������������ �������.                                     
   ��������� ������� ����� ������ ��� ����������, ������ ��� �� ������������ ������� �������������� �������, ����� ������ HTML.
<p><b>�������� ������</b> ��������� ��� �������� ������� �����. ��� HTML, � ������� �� ������ ������������ ��������� ��������� ����:<br><br>
<table border=0 cellpadding=2>
  <tr><td width=100 valign=top><font color=blue>{site_name}</font>
   <td>�������� �����. ��� �������� ���������� � ������� "��������" ������ CMS.
  <tr><td valign=top><font color=blue>{site_url}</font>
   <td>URL �����. �������� ���������� � ������� "��������".
  <tr><td valign=top><font color=blue>{site_mail}</font>
   <td>E-mail ��������������. ������ "��������" CMS.
  <tr><td height=10 colspan=2>
  <tr><td valign=top><font color=blue>{meta}</font>
   <td>�������� Keywords � Description. ������ "��������", � ����� ��. "���������" � ������ ��������.
  <tr><td height=10 colspan=2>
  <tr><td><font color=blue>{banner}</font>
   <td>����� ��� ����������� ���������� �������
  <tr><td><font color=blue>{banners}</font>
   <td>����� ��� ����������� ���� ��������
  <tr><td height=10 colspan=2>
  <tr><td valign=top><font color=blue>{page}</font>
   <td>���������� ��������. ��. ������ "��������" CMS.
  <tr><td valign=top><font color=blue>{page_name}</font>
   <td>�������� ��������. ��. ��������� ��������.
  <tr><td valign=top><font color=blue>{page_title}</font>
   <td>��������� ��������. ��. ��������� ��������.
  <tr><td height=10 colspan=2>
  <tr><td valign=top><font color=blue>{menu}</font>
   <td>����� ��� ����������� ����. ��. �����.
  <tr><td valign=top><font color=blue>{submenu}</font>
   <td>����� ��� ����������� �������. ��. �����.
  <tr><td valign=top><font color=blue>{menu2}</font>
   <td>����� ��� ����������� ������� ����. ��. �����.
  <tr><td valign=top><font color=blue>{news}</font>
   <td>����� ��� ����������� ����� ����������. ��. �����.
  <tr><td valign=top><font color=blue>{news_arc}</font>
   <td>����� ��� ����������� ������ ����������. ��. �����.
  <tr><td valign=top><font color=blue>{news_cats}</font>
   <td>����� ��� ����������� ��������� ���������� ��. �����.
  <tr><td valign=top><font color=blue>{news_cat_path}</font>
   <td>����� ��� ����������� ���� �� ���������� ���������� ��. �����.
  <tr><td valign=top><font color=blue>{path}</font>
   <td>����� ��� ����������� ���� �� ����� ��. �����.
  <tr><td valign=top><font color=blue>{page_header}<br>�<br>{page_footer}</font>
   <td>���� � ��� �������, ���� ����� �� ��� � � ������� �������� �������

</table>
<p><b>���� ������</b> - ��� ���� <font color=darkblue>style.css</font>. ��� ��� ����������� ��� ���� �������� � "�������� �������" ��������� ������:<br><br>
<font color=green>&lt;link href="style.css" type=text/css rel=stylesheet&gt;</font>
<p><b>������ ����</b> ��������� ������� ��� ���� �����. "�������" ������������� �������� ������.
"��������" - ����������� ������ ����, ���������������� ������� �������� �����. � ���� �������� ������������ ��������� ����:<br><br>
<table border=0 cellpadding=2>
  <tr><td width=100 valign=top><font color=blue>{menu_name}</font>
   <td>�������� ������ ����.
  <tr><td valign=top><font color=blue>{menu_url}</font>
   <td>URL, �� ������� ��������� ����� ����.
  <tr><td valign=top><font color=blue>{menu_count}</font>
   <td>����� ���� �� ������� ������� � 1.
  <tr><td valign=top><font color=blue>{submenu}</font>
   <td>����� ��� ����������� �������. ��. �����.
</table>
<p>� <b>�������� �������</b> �� ����� ��� ��. ����:<br><br>
<table border=0 cellpadding=2>
  <tr><td width=100 valign=top><font color=blue>{submenu_name}</font>
   <td>�������� ������ �������.
  <tr><td valign=top><font color=blue>{submenu_url}</font>
   <td>URL, �� ������� ��������� ����� �������.
  <tr><td valign=top><font color=blue>{submenu_count}</font>
   <td>����� ������� �� ������� ������� � 1.
</table>
<p><b>����������</b> ��������� ��� ����� ������������� ���������� �� ��������. ����:<br><br>
<table border=0 cellpadding=2>
  <tr><td width=100 valign=top><font color=blue>{news_header}</font>
   <td>��������� ����������
  <tr><td valign=top><font color=blue>{news_url}</font>
   <td>URL, ������� ��������� �� ����� ����������.
  <tr><td valign=top><font color=blue>{news_date}</font>
   <td>���� ���������� � ������� DD.MM.YYYY
  <tr><td valign=top><font color=blue>{news_time}</font>
   <td>����� ���������� � ������� HH:MM
  <tr><td valign=top><font color=blue>{news_anons}</font>
   <td>������� ����� ����������.
  <tr><td valign=top><font color=blue>{news_author}</font>
   <td>��� ������ ����������.
  <tr><td valign=top><font color=blue>{news_author_mail}</font>
   <td>E-mail ������ ����������.
  <tr><td valign=top><font color=blue>{news_source}</font>
   <td>�������� ��������� ����������.
  <tr><td valign=top><font color=blue>{news_source_url}</font>
   <td>Url ������ �� �������� ����������.
  <tr><td valign=top><font color=blue>{news_print_url}</font>
   <td>Url ������ �� ������ ��� ������.
  <tr><td valign=top><font color=blue>{news_foto1}, {news_foto2}</font>
   <td>Url ������ �� ��������� ��������.
  <tr><td valign=top><font color=blue>{news_count}</font>
   <td>����� ���������� �� ������� ������� � 1.
  <tr><td valign=top><font color=blue>{news_text}</font>
   <td>����� ����������. ���������: ������ ��� ����� �������������� ������ � �������� "����� ����������" � "������ ��� ������".
</table>

<p><b>��������� ����������</b> ��������� ��� ����� ������������� ������ �� ���������(��������) ���������� �� ��������.<br>
<b>���� �� ���������� ����������</b> ��������� ��� ����� ��������� ���� "������� ������" - ���� �� ���������� ���������� �� ��������. ����:<br><br>
<table border=0 cellpadding=2>
  <tr><td width=100 valign=top><font color=blue>{news_cat_name}</font>
   <td>��������� ������
  <tr><td valign=top><font color=blue>{news_cat_url}</font>
   <td>URL, �� ������� ��������� ���������.
</table>
<p><b>���� �� �����</b> ��������� ��� ����� �������������  ���� "������� ������" - ���� �� ����� �� ��������.<br>
<table border=0 cellpadding=2>
  <tr><td width=100 valign=top><font color=blue>{path_name}</font>
   <td>��������� ������
  <tr><td valign=top><font color=blue>{path_url}</font>
   <td>URL, �� ������� ��������� ���������.
</table>

<p>��� ����, ����� ����� ����������������� ������ ����������� � �����, ������� ������ <b>��������</b>. ����� ������������� ��� ����������� �������� �� �������� ��������, � ���� ����� �����!
<p>
</body></html>
