<HTML><HEAD><TITLE>HTML Code Cleanup</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<META http-equiv=Pragma content=no-cache><LINK 
href="HTML Code Cleanup.files/dialog.css" type=text/css rel=stylesheet>
<SCRIPT language=javascript src="HTML Code Cleanup.files/utils.js"></SCRIPT>

<SCRIPT language=javascript>
function Init()
{
	resizeDialogToContent();
	window.focus();
}

function okClick()
{
	var cprops = {};
	if (cleanupHTML.cleantype[0].checked) {cprops.opt1=1;cprops.opt2=1;cprops.opt3=1;cprops.opt4=1;cprops.opt5=1;}
	if (cleanupHTML.cleantype[1].checked) {cprops.opt1=0;cprops.opt2=1;cprops.opt3=0;cprops.opt4=0;cprops.opt5=0;}
	if (cleanupHTML.cleantype[2].checked) {cprops.opt1=0;cprops.opt2=0;cprops.opt3=1;cprops.opt4=0;cprops.opt5=0;}
	if (cleanupHTML.cleantype[3].checked) {cprops.opt1=0;cprops.opt2=0;cprops.opt3=0;cprops.opt4=1;cprops.opt5=0;}

	window.returnValue = cprops;
	window.close();
}
</SCRIPT>

<META content="MSHTML 6.00.2600.0" name=GENERATOR></HEAD>
<BODY onload=Init()>
<FORM name=cleanupHTML>
<DIV class=txt 
style="BORDER-RIGHT: black 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 1px solid; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: black 1px solid; WIDTH: 350px; PADDING-TOP: 5px; BORDER-BOTTOM: black 1px solid; BACKGROUND-COLOR: #ffffff">Choose 
a cleanup code option. All or only a part of your formatting may be lost.</DIV>
<DIV 
style="BORDER-RIGHT: black 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: transparent; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: black 1px solid; WIDTH: 350px; PADDING-TOP: 5px; BORDER-BOTTOM: black 1px solid">
<TABLE cellSpacing=0 cellPadding=2 border=0>
  <TBODY>
  <TR>
    <TD><INPUT class=bt type=radio CHECKED name=cleantype></TD>
    <TD class=txt>&nbsp;<B>All the unused HTML tags<BR>(adviced after a 
      Copy/Paste from WORD)</B></TD></TR>
  <TR>
    <TD><INPUT class=bt type=radio name=cleantype></TD>
    <TD class=txt>&nbsp;<B>The styles (&lt;CSS&gt;)</B></TD></TR>
  <TR>
    <TD><INPUT class=bt type=radio name=cleantype></TD>
    <TD class=txt>&nbsp;<B>The Fonts (&lt;FONT&gt;)</B></TD></TR>
  <TR>
    <TD><INPUT class=bt type=radio name=cleantype></TD>
    <TD class=txt>&nbsp;<B>The &lt;SPAN&gt; 
tags</B></TD></TR></TBODY></TABLE></DIV><BR>
<DIV style="WIDTH: 350px; TEXT-ALIGN: right"><INPUT class=bt onfocus=if(this.blur)this.blur() onclick=okClick() type=button value="   OK   "> 
<INPUT class=bt onfocus=if(this.blur)this.blur() onclick=window.close() type=button value=Cancel><BR><BR></DIV></FORM></BODY></HTML>
