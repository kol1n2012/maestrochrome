<HTML><HEAD><TITLE>Cell Properties</TITLE>
<META http-equiv=Pragma content=no-cache>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
href="Cell Properties.files/dialog.css" type=text/css rel=stylesheet><LINK 
href="Cell Properties.files/toolbar.css" type=text/css rel=stylesheet>
<SCRIPT language=javascript src="Cell Properties.files/toolbar.js.php"></SCRIPT>

<SCRIPT language=javascript src="Cell Properties.files/utils.js"></SCRIPT>

<SCRIPT language=javascript>
function callColorDlg()
{
	var sColor = dlgHelper.ChooseColorDlg();
	sColor = sColor.toString(16);	//change decimal to hex

	if (sColor.length < 6)	//add extra zeroes if hex number is less than 6 digits
	{
		var sTempString = "000000".substring(0,6-sColor.length);
		sColor = sTempString.concat(sColor);
	}
	return sColor;
}
function showColorPicker(curcolor) {
	var newcol = callColorDlg();
	try {
		td_prop.cbgcolor.value = newcol;
		td_prop.color_sample.style.backgroundColor = td_prop.cbgcolor.value;
	}
	catch (excp) {}
}

function Init()
{
	var cProps = window.dialogArguments;
	if (cProps)
	{
		// set attribute values
		td_prop.cbgcolor.value = cProps.bgColor;
		td_prop.color_sample.style.backgroundColor = td_prop.cbgcolor.value;
		if (cProps.width)
		{
			if (!isNaN(cProps.width) || (cProps.width.substr(cProps.width.length-2,2).toLowerCase() == "px"))
			{
				// pixels
				if (!isNaN(cProps.width))
				  td_prop.cwidth.value = cProps.width;
				else
				  td_prop.cwidth.value = cProps.width.substr(0,cProps.width.length-2);
				td_prop.cwunits.options[0].selected = false;
				td_prop.cwunits.options[1].selected = true;
			}
			else
			{
				// percents
				td_prop.cwidth.value = cProps.width.substr(0,cProps.width.length-1);
				td_prop.cwunits.options[0].selected = true;
				td_prop.cwunits.options[1].selected = false;
			}
		}
		if (cProps.height)
		{
			if (!isNaN(cProps.height) || (cProps.height.substr(cProps.height.length-2,2).toLowerCase() == "px"))
			{
				// pixels
				if (!isNaN(cProps.height))
				  td_prop.cheight.value = cProps.height;
				else
				  td_prop.cheight.value = cProps.height.substr(0,cProps.height.length-2);
				td_prop.chunits.options[0].selected = false;
				td_prop.chunits.options[1].selected = true;
			}
			else
			{
				// percents
				td_prop.cheight.value = cProps.height.substr(0,cProps.height.length-1);
				td_prop.chunits.options[0].selected = true;
				td_prop.chunits.options[1].selected = false;
			}
		}

		setHAlign(cProps.align);
		setVAlign(cProps.vAlign);

		if (cProps.noWrap) td_prop.cnowrap.checked = true;

		if (cProps.styleOptions)
		{
			for (i=1; i<cProps.styleOptions.length; i++)
			{
				var oOption = document.createElement("OPTION");
				td_prop.ccssclass.add(oOption);
				oOption.innerText = cProps.styleOptions[i].innerText;
				oOption.value = cProps.styleOptions[i].value;

				if (cProps.className) td_prop.ccssclass.value = cProps.className;
			}
		}
	}
	resizeDialogToContent();
}

function validateParams()
{
	// check width and height
	if (isNaN(parseInt(td_prop.cwidth.value)) && td_prop.cwidth.value != '')
	{
		alert('Error: Width is not a number');
		td_prop.cwidth.focus();
		return false;
	}
	if (isNaN(parseInt(td_prop.cheight.value)) && td_prop.cheight.value != '')
	{
		alert('Error: Height is not a number');
		td_prop.cheight.focus();
		return false;
	}
    return true;
}

function okClick()
{
	// validate paramters
	if (validateParams())    
	{
		var cprops = {};
		cprops.align = (td_prop.chalign.value)?(td_prop.chalign.value):'';
		cprops.vAlign = (td_prop.cvalign.value)?(td_prop.cvalign.value):'';
		cprops.width = (td_prop.cwidth.value)?(td_prop.cwidth.value + td_prop.cwunits.value):'';
		cprops.height = (td_prop.cheight.value)?(td_prop.cheight.value + td_prop.chunits.value):'';
		cprops.bgColor = td_prop.cbgcolor.value;
		cprops.className = (td_prop.ccssclass.value != 'default')?td_prop.ccssclass.value:'';
		cprops.noWrap = (td_prop.cnowrap.checked)?true:false;

		window.returnValue = cprops;
		window.close();
	}
}

function setSample()
{
	try { td_prop.color_sample.style.backgroundColor = td_prop.cbgcolor.value; }
	catch (excp) {}
}

function setHAlign(alignment)
{
	switch (alignment)
	{
		case "left":
			td_prop.ha_left.className = "span_WXEDIT_default_tb_down";
			td_prop.ha_center.className = "";
			td_prop.ha_right.className = "";
			break;
		case "center":
			td_prop.ha_left.className = "";
			td_prop.ha_center.className = "span_WXEDIT_default_tb_down";
			td_prop.ha_right.className = "";
			break;
		case "right":
			td_prop.ha_left.className = "";
			td_prop.ha_center.className = "";
			td_prop.ha_right.className = "span_WXEDIT_default_tb_down";
			break;
	}
	td_prop.chalign.value = alignment;
}

function setVAlign(alignment)
{
	switch (alignment) {
      case "top":
        td_prop.ha_top.className = "span_WXEDIT_default_tb_down";
        td_prop.ha_middle.className = "";
        td_prop.ha_bottom.className = "";
        td_prop.ha_baseline.className = "";
        break;
      case "middle":
        td_prop.ha_top.className = "";
        td_prop.ha_middle.className = "span_WXEDIT_default_tb_down";
        td_prop.ha_bottom.className = "";
        td_prop.ha_baseline.className = "";
        break;
      case "bottom":
        td_prop.ha_top.className = "";
        td_prop.ha_middle.className = "";
        td_prop.ha_bottom.className = "span_WXEDIT_default_tb_down";
        td_prop.ha_baseline.className = "";
        break;
      case "baseline":
        td_prop.ha_top.className = "";
        td_prop.ha_middle.className = "";
        td_prop.ha_bottom.className = "";
        td_prop.ha_baseline.className = "span_WXEDIT_default_tb_down";
        break;
	}
	td_prop.cvalign.value = alignment;
}
</SCRIPT>

<META content="MSHTML 6.00.2600.0" name=GENERATOR></HEAD>
<BODY onload=Init()>
<OBJECT id=dlgHelper height=0px width=0px 
classid=clsid:3050f819-98b5-11cf-bb82-00aa00bdce0b></OBJECT>
<TABLE cellSpacing=0 cellPadding=2 width=336 border=0>
  <FORM name=td_prop>
  <TBODY>
  <TR>
    <TD class=txt>Horizontal align:</TD>
    <TD align=right><INPUT type=hidden name=chalign></TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            class=dialog_WXEDIT_default_tb_out 
            onmousedown=dialog_WXEDIT_default_bt_down(this) id=ha_left 
            onmouseover=dialog_WXEDIT_default_bt_over(this) 
            onclick="setHAlign('left');" 
            onmouseout=dialog_WXEDIT_default_bt_out(this) alt=Left 
            src="Cell Properties.files/tb_left.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            class=dialog_WXEDIT_default_tb_out 
            onmousedown=dialog_WXEDIT_default_bt_down(this) id=ha_center 
            onmouseover=dialog_WXEDIT_default_bt_over(this) 
            onclick="setHAlign('center');" 
            onmouseout=dialog_WXEDIT_default_bt_out(this) alt=Left 
            src="Cell Properties.files/tb_center.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            class=dialog_WXEDIT_default_tb_out 
            onmousedown=dialog_WXEDIT_default_bt_down(this) id=ha_right 
            onmouseover=dialog_WXEDIT_default_bt_over(this) 
            onclick="setHAlign('right');" 
            onmouseout=dialog_WXEDIT_default_bt_out(this) alt=Left 
            src="Cell Properties.files/tb_right.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD class=txt>Vertical align:</TD>
    <TD align=right><INPUT type=hidden name=cvalign></TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            class=dialog_WXEDIT_default_tb_out 
            onmousedown=dialog_WXEDIT_default_bt_down(this) id=ha_top 
            onmouseover=dialog_WXEDIT_default_bt_over(this) 
            onclick="setVAlign('top');" 
            onmouseout=dialog_WXEDIT_default_bt_out(this) alt=Top 
            src="Cell Properties.files/tb_top.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            class=dialog_WXEDIT_default_tb_out 
            onmousedown=dialog_WXEDIT_default_bt_down(this) id=ha_middle 
            onmouseover=dialog_WXEDIT_default_bt_over(this) 
            onclick="setVAlign('middle');" 
            onmouseout=dialog_WXEDIT_default_bt_out(this) alt=Middle 
            src="Cell Properties.files/tb_middle.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            class=dialog_WXEDIT_default_tb_out 
            onmousedown=dialog_WXEDIT_default_bt_down(this) id=ha_bottom 
            onmouseover=dialog_WXEDIT_default_bt_over(this) 
            onclick="setVAlign('bottom');" 
            onmouseout=dialog_WXEDIT_default_bt_out(this) alt=Bottom 
            src="Cell Properties.files/tb_bottom.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            class=dialog_WXEDIT_default_tb_out 
            onmousedown=dialog_WXEDIT_default_bt_down(this) id=ha_baseline 
            onmouseover=dialog_WXEDIT_default_bt_over(this) 
            onclick="setVAlign('baseline');" 
            onmouseout=dialog_WXEDIT_default_bt_out(this) alt=Baseline 
            src="Cell Properties.files/tb_baseline.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=2 width=10 border=0>
  <TBODY>
  <TR>
    <TD width=10></TD>
    <TD width=10></TD>
    <TD width=10></TD>
  <TR>
    <TD class=txt noWrap>Width :</TD>
    <TD noWrap><INPUT class=input_small size=3 name=cwidth maxlenght="3"> 
      <SELECT class=input size=1 name=cwunits> <OPTION value=% 
        selected>%</OPTION> <OPTION value=px>px</OPTION></SELECT> </TD></TR>
  <TR>
    <TD class=txt noWrap>Height :</TD>
    <TD noWrap><INPUT class=input_small size=3 name=cheight maxlenght="3"> 
      <SELECT class=input size=1 name=chunits> <OPTION value=% 
        selected>%</OPTION> <OPTION value=px>px</OPTION></SELECT> </TD></TR>
  <TR>
    <TD class=txt noWrap>CSS class :</TD>
    <TD noWrap colSpan=3><SELECT class=input id=ccssclass size=1 
      name=ccssclass></SELECT></TD></TR>
  <TR>
    <TD class=txt noWrap colSpan=2>No wrap</TD>
    <TD>:</TD>
    <TD><INPUT onfocus=if(this.blur)this.blur() type=checkbox align=absMiddle 
      name=cnowrap></TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=2 border=0>
  <TBODY>
  <TR>
    <TD class=txt>Background color: <IMG id=color_sample height=16 
      src="Cell Properties.files/spacer.gif" width=30 align=absBottom 
      border=1>&nbsp;<INPUT class=input_color onkeyup=setSample() size=7 
      name=cbgcolor maxlenght="7">&nbsp;</TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            onmouseup=WXEDIT_default_bt_up(this) class=WXEDIT_default_tb_out 
            onmousedown=WXEDIT_default_bt_down(this) id=colorpicker 
            onmouseover=WXEDIT_default_bt_over(this) 
            onclick=showColorPicker(cbgcolor.value) 
            onmouseout=WXEDIT_default_bt_out(this) alt="Background color" 
            src="Cell Properties.files/tb_colorpicker.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=2 width="100%" border=0>
  <TBODY>
  <TR>
    <TD noWrap colSpan=4>
      <HR width="100%">
    </TD></TR>
  <TR>
    <TD noWrap align=right colSpan=4><INPUT class=bt onfocus=if(this.blur)this.blur() onclick=okClick() type=button value="   OK   "> 
<INPUT class=bt onfocus=if(this.blur)this.blur() onclick=window.close() type=button value=Cancel> 
    </TD></TR></FORM></TBODY></TABLE></BODY></HTML>
