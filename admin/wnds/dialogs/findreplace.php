<HTML><HEAD><TITLE>Find / Replace</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<META http-equiv=Pragma content=no-cache><LINK 
href="Find - Replace.files/dialog.css" type=text/css rel=stylesheet>
<SCRIPT language=javascript src="Find - Replace.files/utils.js"></SCRIPT>

<SCRIPT language=javascript>
var rng;
rng = dialogArguments[0].document.selection.createRange();

function Init() {resizeDialogToContent();}

// Retourne les valeurs pour "matching case" et "matching whole words"
function searchtype()
{
    var retval = 0;
    var matchcase = 0;
    var matchword = 0;
    if (document.frmSearch.blnMatchCase.checked) matchcase = 4;
    if (document.frmSearch.blnMatchWord.checked) matchword = 2;
    retval = matchcase + matchword;
    return(retval);
}

function checkInput()
{
	if (document.frmSearch.strSearch.value.length < 1)
	{
        alert("Please enter text to find.");
        return false;
    }
	else return true;
}

// Trouver le texte
function findtext(){
    if (checkInput()) {
        var searchval = document.frmSearch.strSearch.value;
        rng.collapse(false);
        if (rng.findText(searchval, 1000000000, searchtype())) {
            rng.select();
        } else {
            var startfromtop = confirm("Your word was not found. Woud you like to start again from the top ?");
            if (startfromtop) {
                rng.expand("textedit");
                rng.collapse();
                rng.select();
                findtext();
            }
        }
    }
}

// Remplacer le texte sélectionné
function replacetext()
{
	if (checkInput())
	{
		if (document.frmSearch.blnMatchCase.checked)
		{
			if (rng.text == document.frmSearch.strSearch.value) rng.text = document.frmSearch.strReplace.value
		}
		else
		{
            if (rng.text.toLowerCase() == document.frmSearch.strSearch.value.toLowerCase()) rng.text = document.frmSearch.strReplace.value
        }
        findtext();
    }
}

function replacealltext()
{
	if (checkInput())
	{
		var searchval = document.frmSearch.strSearch.value;
		var wordcount = 0;
		var msg = "";
		rng.expand("textedit");
		rng.collapse();
		rng.select();
		while (rng.findText(searchval, 1000000000, searchtype()))
		{
			rng.select();
			rng.text = document.frmSearch.strReplace.value;
			wordcount++;
        }
		if (wordcount == 0) msg = "Word was not found. Nothing was replaced."
		else msg = wordcount + " word(s) were replaced.";
		alert(msg);
    }
}
</SCRIPT>

<META content="MSHTML 6.00.2600.0" name=GENERATOR></HEAD>
<BODY onload=Init()>
<FORM name=frmSearch action="" method=post>
<TABLE cellSpacing=0 cellPadding=5 border=0>
  <TBODY>
  <TR>
    <TD style="FONT-SIZE: 11px; FONT-FAMILY: Arial" vAlign=top noWrap 
      align=left><LABEL for=strSearch>Find what :</LABEL><BR><INPUT class=input 
      id=strSearch style="WIDTH: 280px" size=40 name=strSearch><BR><LABEL 
      for=strReplace>Replace with :</LABEL><BR><INPUT class=input id=strReplace 
      style="WIDTH: 280px" size=40 name=strReplace><BR><INPUT id=blnMatchCase 
      style="MARGIN-TOP: 3px; WIDTH: 14px; MARGIN-RIGHT: 3px; HEIGHT: 14px" 
      onfocus=if(this.blur)this.blur() type=checkbox size=40 
      name=blnMatchCase><LABEL for=blnMatchCase>Match case</LABEL><BR><INPUT 
      id=blnMatchWord 
      style="MARGIN-TOP: 3px; WIDTH: 14px; MARGIN-RIGHT: 3px; HEIGHT: 14px" 
      onfocus=if(this.blur)this.blur() type=checkbox size=40 
      name=blnMatchWord><LABEL for=blnMatchWord>Match whole word only</LABEL> 
</TD>
    <TD vAlign=top rowSpan=2><BUTTON class=bt 
      style="MARGIN-TOP: 15px; WIDTH: 120px" onfocus=if(this.blur)this.blur() 
      onclick=findtext(); name=btnFind>Find Next</BUTTON><BR><BUTTON class=bt 
      style="MARGIN-TOP: 5px; WIDTH: 120px" onfocus=if(this.blur)this.blur() 
      onclick=window.close(); name=btnCancel>Close</BUTTON><BR><BUTTON class=bt 
      style="MARGIN-TOP: 5px; WIDTH: 120px" onfocus=if(this.blur)this.blur() 
      onclick=replacetext(); name=btnReplace>Replace</BUTTON><BR><BUTTON 
      class=bt style="MARGIN-TOP: 5px; WIDTH: 120px" 
      onfocus=if(this.blur)this.blur() onclick=replacealltext(); 
      name=btnReplaceall>Replace 
All</BUTTON><BR></TD></TR></TBODY></TABLE></FORM></BODY></HTML>
