<HTML><HEAD><TITLE>Image properties</TITLE>
<META http-equiv=Pragma content=no-cache>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
href="Image properties.files/dialog.css" type=text/css rel=stylesheet>
<SCRIPT language=javascript src="Image properties.files/utils.js"></SCRIPT>

<SCRIPT language=javascript>
var Fimage = new Image;

function Init()
{
	var iProps = window.dialogArguments;
	if (iProps)
	{
		// set attribute values
		if (iProps.width) {img_prop.cwidth.value = iProps.width;}
		if (iProps.height) {img_prop.cheight.value = iProps.height;}
		setAlign(iProps.align);
      
		if (iProps.src) {img_prop.csrc.value = iProps.src; imgpreview.document.body.innerHTML = "<img name=\"Ifrimg\" src=\""+iProps.src+"\">";}
		if (iProps.alt) {img_prop.calt.value = iProps.alt;}
		if (iProps.border) {img_prop.cborder.value = iProps.border;}
		if (iProps.hspace) {img_prop.chspace.value = iProps.hspace;}
		if (iProps.vspace) {img_prop.cvspace.value = iProps.vspace;}
		Fimage.src = img_prop.csrc.value;
	}
	resizeDialogToContent();
}

function validateParams()
{
	// check width and height
	if (isNaN(parseInt(img_prop.cwidth.value)) && img_prop.cwidth.value != '')
	{
		alert('Error: Width is not a number');
		img_prop.cwidth.focus();
		return false;
	}
	if (isNaN(parseInt(img_prop.cheight.value)) && img_prop.cheight.value != '')
	{
		alert('Error: Height is not a number');
		img_prop.cheight.focus();
		return false;
	}
	if (isNaN(parseInt(img_prop.cborder.value)) && img_prop.cborder.value != '')
	{
		alert('Error: Border is not a number');
		img_prop.cborder.focus();
		return false;
	}
	if (isNaN(parseInt(img_prop.chspace.value)) && img_prop.chspace.value != '')
	{
		alert('Error: Horizontal space is not a number');
		img_prop.chspace.focus();
		return false;
	}
	if (isNaN(parseInt(img_prop.cvspace.value)) && img_prop.cvspace.value != '')
	{
		alert('Error: Vertical space is not a number');
		img_prop.cvspace.focus();
		return false;
	}
	return true;
}

function okClick()
{
	// validate paramters
	if (validateParams())    
	{
		var iProps = {};
		iProps.align = (img_prop.calign.value)?(img_prop.calign.value):'';
		iProps.width = (img_prop.cwidth.value)?(img_prop.cwidth.value):'';
		iProps.height = (img_prop.cheight.value)?(img_prop.cheight.value):'';
		iProps.border = (img_prop.cborder.value)?(img_prop.cborder.value):'';
		iProps.src = (img_prop.csrc.value)?(img_prop.csrc.value):'';
		iProps.alt = (img_prop.calt.value)?(img_prop.calt.value):'';
		iProps.hspace = (img_prop.chspace.value)?(img_prop.chspace.value):'';
		iProps.vspace = (img_prop.cvspace.value)?(img_prop.cvspace.value):'';

		window.returnValue = iProps;
		window.close();
	}
}

function changesize(tx)
{
	if (img_prop.constrain.checked==true)
		img_prop.cheight.value = Math.round((tx/Fimage.width)*Fimage.height)
}

function setAlign(alignment)
{
	for (i=0; i<img_prop.calign.options.length; i++)  
	{
		al = img_prop.calign.options.item(i);
		if (al.value == alignment.toLowerCase())
		{
			img_prop.calign.selectedIndex = al.index;
		}
	}
}
</SCRIPT>

<META content="MSHTML 6.00.2600.0" name=GENERATOR></HEAD>
<BODY onload=Init()>
<FORM name=img_prop>
<TABLE cellSpacing=0 cellPadding=0 border=0>
  <TBODY>
  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=2 border=0>
        <TBODY>
        <TR>
          <TD width=10></TD>
          <TD width=10></TD>
          <TD width=10></TD>
          <TD width=10></TD>
          <TD width=10></TD></TR>
        <TR>
          <TD class=txt>Source</TD>
          <TD>:</TD>
          <TD colSpan=3><INPUT class=input 
            onchange="imgpreview.document.body.innerHTML='<img src='+this.value+'>';" 
            size=32 name=csrc></TD></TR>
        <TR>
          <TD class=txt>Alternative text</TD>
          <TD>:</TD>
          <TD colSpan=3><INPUT class=input size=32 name=calt></TD></TR>
        <TR>
          <TD class=txt>Align</TD>
          <TD>:</TD>
          <TD align=left colSpan=3><SELECT class=input size=1 name=calign> 
              <OPTION value="" selected></OPTION> <OPTION 
              value=left>Left</OPTION> <OPTION value=right>Right</OPTION> 
              <OPTION value=top>Top</OPTION> <OPTION 
              value=middle>Middle</OPTION> <OPTION value=bottom>Bottom</OPTION> 
              <OPTION value=absmiddle>Absmiddle</OPTION> <OPTION 
              value=texttop>Texttop</OPTION> <OPTION 
              value=baseline>Baseline</OPTION></SELECT> </TD></TR>
        <TR>
          <TD class=txt>Border</TD>
          <TD>:</TD>
          <TD colSpan=3><INPUT class=input_small name=cborder></TD></TR>
        <TR>
          <TD class=txt>Width</TD>
          <TD>:</TD>
          <TD colSpan=3><INPUT class=input_small 
            onchange=changesize(this.value); size=3 name=cwidth 
        maxlenght="3"></TD></TR>
        <TR>
          <TD class=txt>Height</TD>
          <TD>:</TD>
          <TD colSpan=3><INPUT class=input_small size=3 name=cheight 
            maxlenght="3"></TD></TR>
        <TR>
          <TD class=txt noWrap colSpan=3>Constrain Proportions</TD>
          <TD>:</TD>
          <TD><INPUT onfocus=if(this.blur)this.blur() 
            onclick=changesize(img_prop.cwidth.value); type=checkbox 
            name=constrain></TD></TR>
        <TR>
          <TD class=txt noWrap colSpan=3>Hor. space</TD>
          <TD>:</TD>
          <TD><INPUT class=input_small size=3 name=chspace 
maxlenght="3"></TD></TR>
        <TR>
          <TD class=txt noWrap colSpan=3>Vert. space</TD>
          <TD>:</TD>
          <TD><INPUT class=input_small size=3 name=cvspace 
maxlenght="3"></TD></TR>
        <TR>
          <TD colSpan=5>
            <HR width="100%">
          </TD></TR>
        <TR>
          <TD vAlign=bottom align=right colSpan=5><INPUT class=bt onfocus=if(this.blur)this.blur() onclick=okClick() type=button value="   OK   "> 
<INPUT class=bt onfocus=if(this.blur)this.blur() onclick=window.close() type=button value=Cancel> 
          </TD></TR></TBODY></TABLE></TD>
    <TD><IFRAME style="WIDTH: 200px; HEIGHT: 260px" name=imgpreview 
      marginWidth=0 marginHeight=0 src="about:blank" 
  frameBorder=0></IFRAME></TD></TR></TBODY></TABLE></FORM></BODY></HTML>
