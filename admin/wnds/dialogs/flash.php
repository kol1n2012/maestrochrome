<HTML><HEAD><TITLE>Flash Object Properties</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<META http-equiv=Pragma content=no-cache><LINK href="flash.files/dialog.css" 
type=text/css rel=stylesheet><LINK href="flash.files/toolbar.css" type=text/css 
rel=stylesheet>
<SCRIPT language=javascript src="flash.files/toolbar.js.php"></SCRIPT>

<SCRIPT language=javascript src="flash.files/utils.js"></SCRIPT>

<SCRIPT language=javascript>
function callColorDlg()
{
	var sColor = dlgHelper.ChooseColorDlg();
	sColor = sColor.toString(16);	//change decimal to hex

	if (sColor.length < 6)	//add extra zeroes if hex number is less than 6 digits
	{
		var sTempString = "000000".substring(0,6-sColor.length);
		sColor = sTempString.concat(sColor);
	}
	return sColor;
}

function showColorPicker(curcolor)
{
	var newcol = callColorDlg();
	try
	{
		flash_prop.cbgcolor.value = newcol;
		flash_prop.color_sample.style.backgroundColor = flash_prop.cbgcolor.value;
	}
	catch (excp) {}
}

function Init()
{
	var cProps = window.dialogArguments;
	if (cProps)
	{
		flash_prop.valueflash.value = cProps.src;
		flash_prop.qualityflash.value = (cProps.quality == 1)?'High':'Low'; 
		flash_prop.cbgcolor.value = cProps.bgColor;
		flash_prop.color_sample.style.backgroundColor = flash_prop.cbgcolor.value;
		if (cProps.width)
		{
			if (!isNaN(cProps.width) || (cProps.width.substr(cProps.width.length-2,2).toLowerCase() == "px"))
			{
				// pixels
				if (!isNaN(cProps.width))
				  flash_prop.cwidth.value = cProps.width;
				else
				  flash_prop.cwidth.value = cProps.width.substr(0,cProps.width.length-2);
				flash_prop.cwunits.options[0].selected = false;
				flash_prop.cwunits.options[1].selected = true;
			}
			else
			{
				// percents
				flash_prop.cwidth.value = cProps.width.substr(0,cProps.width.length-1);
				flash_prop.cwunits.options[0].selected = true;
				flash_prop.cwunits.options[1].selected = false;
			}
		}
		if (cProps.height)
		{
			if (!isNaN(cProps.height) || (cProps.height.substr(cProps.height.length-2,2).toLowerCase() == "px"))
			{
				// pixels
				if (!isNaN(cProps.height))
				  flash_prop.cheight.value = cProps.height;
				else
				  flash_prop.cheight.value = cProps.height.substr(0,cProps.height.length-2);
				flash_prop.chunits.options[0].selected = false;
				flash_prop.chunits.options[1].selected = true;
			}
			else
			{
				// percents
				flash_prop.cheight.value = cProps.height.substr(0,cProps.height.length-1);
				flash_prop.chunits.options[0].selected = true;
				flash_prop.chunits.options[1].selected = false;
			}
		}
	}
	resizeDialogToContent();
}
  
function validateParams()
{
	// check width and height
	if (isNaN(parseInt(flash_prop.cwidth.value)) && flash_prop.cwidth.value != '')
	{
		alert('Error: Non numerical width');
		flash_prop.cwidth.focus();
		return false;
	}
	if (isNaN(parseInt(flash_prop.cheight.value)) && flash_prop.cheight.value != '')
	{
		alert('Error: Non numerical hieght');
		flash_prop.cheight.focus();
		return false;
	}
	return true;
}
  
function okClick()
{
	// validate paramters
	if (validateParams())    
	{
		var cprops = {};
		cprops.src = (flash_prop.valueflash.value)?(flash_prop.valueflash.value):'';
		cprops.width = (flash_prop.cwidth.value)?(flash_prop.cwidth.value + flash_prop.cwunits.value):'';
		cprops.height = (flash_prop.cheight.value)?(flash_prop.cheight.value + flash_prop.chunits.value):'';
		cprops.quality = (flash_prop.qualityflash.value == 'High')?1:0;
		cprops.bgColor = flash_prop.cbgcolor.value;

		window.returnValue = cprops;
		window.close();
	}
}

function setSample()
{
	try {flash_prop.color_sample.style.backgroundColor = flash_prop.cbgcolor.value;}
	catch (excp) {}
}
</SCRIPT>

<META content="MSHTML 6.00.2600.0" name=GENERATOR></HEAD>
<BODY onload=Init()>
<OBJECT id=dlgHelper height=0px width=0px 
classid=clsid:3050f819-98b5-11cf-bb82-00aa00bdce0b></OBJECT>
<TABLE cellSpacing=0 cellPadding=2 width="100%" border=0>
  <FORM name=flash_prop>
  <TBODY>
  <TR>
    <TD class=txt>Source</TD>
    <TD>:</TD>
    <TD colSpan=2><INPUT class=input size=50 name=valueflash></TD></TR>
  <TR>
    <TD class=txt>Width</TD>
    <TD>:</TD>
    <TD><INPUT class=input size=3 name=cwidth maxlenght="3"> <SELECT 
      class=input size=1 name=cwunits> <OPTION value=% selected>%</OPTION> 
        <OPTION value=px>px</OPTION></SELECT> </TD>
    <TD class=txt style="PADDING-RIGHT: 10px" noWrap align=right>Quality : 
      <SELECT class=input id=qualityflash size=1 name=qualityflash> <OPTION 
        value=High selected>High</OPTION> <OPTION value=Low>Low</OPTION></SELECT> 
    </TD></TR>
  <TR>
    <TD class=txt>Height</TD>
    <TD>:</TD>
    <TD colSpan=2><INPUT class=input size=3 name=cheight maxlenght="3"> 
      <SELECT class=input size=1 name=chunits> <OPTION value=% 
        selected>%</OPTION> <OPTION value=px>px</OPTION></SELECT> 
</TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=0 border=0>
  <TBODY>
  <TR>
    <TD class=txt>Background Color: <IMG id=color_sample height=16 
      src="flash.files/spacer.gif" width=30 align=absBottom 
      border=1>&nbsp;<INPUT class=input_color onkeyup=setSample() size=7 
      name=cbgcolor maxlenght="7">&nbsp;</TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            onmouseup=WXEDIT_default_bt_up(this) class=WXEDIT_default_tb_out 
            onmousedown=WXEDIT_default_bt_down(this) id=colorpicker 
            onmouseover=WXEDIT_default_bt_over(this) 
            onclick=showColorPicker(cbgcolor.value) 
            onmouseout=WXEDIT_default_bt_out(this) alt="Background Color" 
            src="flash.files/tb_colorpicker.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=2 width="100%" border=0>
  <TBODY>
  <TR>
    <TD noWrap colSpan=2>
      <HR width="100%">
    </TD></TR>
  <TR>
    <TD vAlign=bottom noWrap align=right colSpan=2><INPUT class=bt onfocus=if(this.blur)this.blur() onclick=okClick() type=button value="   OK   "> 
<INPUT class=bt onfocus=if(this.blur)this.blur() onclick=window.close() type=button value=Cancel> 
    </TD></TR></FORM></TBODY></TABLE></BODY></HTML>
