<HTML><HEAD><TITLE>Hyperlink Properties</TITLE>
<META http-equiv=Pragma content=no-cache>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
href="Hyperlink Properties.files/dialog.css" type=text/css rel=stylesheet>
<SCRIPT language=javascript src="Hyperlink Properties.files/utils.js"></SCRIPT>

<SCRIPT language=javascript>
function Init()
{
	var cProps = window.dialogArguments;
	if (cProps)
	{
		if (cProps.ltype) {a_prop.typelink.value = cProps.ltype;}
		if (cProps.href) {a_prop.valuelink.value = cProps.href;} else {a_prop.valuelink.value = "http://";}
		if (cProps.target) {a_prop.targetlink.value = cProps.target;}
		if (cProps.styleOptions)
		{
			for (i=1; i<cProps.styleOptions.length; i++)
			{
				var oOption = document.createElement("OPTION");
				a_prop.classlink.add(oOption);
				oOption.innerText = cProps.styleOptions[i].innerText;
				oOption.value = cProps.styleOptions[i].value;

				if (cProps.className) {a_prop.classlink.value = cProps.className;}
			}
		}
	}
	resizeDialogToContent();
}

function okClick()
{
	var cprops = {};
	cprops.href = (a_prop.valuelink.value)?(a_prop.valuelink.value):'';
	cprops.target = (a_prop.targetlink.value != 'default')?a_prop.targetlink.value:'';
	cprops.className = (a_prop.classlink.value != 'default')?a_prop.classlink.value:'';

	window.returnValue = cprops;
	window.close();
}
</SCRIPT>

<META content="MSHTML 6.00.2600.0" name=GENERATOR></HEAD>
<BODY onload=Init()>
<TABLE cellSpacing=0 cellPadding=2 width="100%" border=0>
  <FORM name=a_prop>
  <TBODY>
  <TR>
    <TD noWrap align=right><SELECT class=input id=typelink 
      onchange=document.a_prop.valuelink.value=this.options[this.selectedIndex].value 
      size=1 name=typelink> <OPTION value=http:// selected>http://</OPTION> 
        <OPTION value=mailto:>mailto:</OPTION> <OPTION value=/>Fichier:</OPTION> 
        <OPTION value=ftp://>ftp://</OPTION></SELECT> </TD>
    <TD noWrap><INPUT class=input size=50 name=valuelink></TD></TR>
  <TR>
    <TD class=txt noWrap align=right>Target:</TD>
    <TD noWrap><SELECT class=input id=targetlink size=1 name=targetlink> 
        <OPTION value=default selected>Default</OPTION> <OPTION value=_blank>New 
        Window</OPTION> <OPTION value=_self>Same Window</OPTION> <OPTION 
        value=_parent>Parent Window</OPTION></SELECT> </TD></TR>
  <TR>
    <TD class=txt noWrap align=right>CSS Class:</TD>
    <TD noWrap><SELECT class=input id=classlink size=1 
    name=classlink></SELECT></TD></TR>
  <TR>
    <TD noWrap colSpan=4>
      <HR width="100%">
    </TD></TR>
  <TR>
    <TD noWrap align=right colSpan=2><INPUT class=bt onfocus=if(this.blur)this.blur() onclick=okClick() type=button value="   OK   "> 
<INPUT class=bt onfocus=if(this.blur)this.blur() onclick=window.close() type=button value=Cancel> 
    </TD></TR></FORM></TBODY></TABLE></BODY></HTML>
