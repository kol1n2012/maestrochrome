<HTML><HEAD><TITLE>Row Properties</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<META http-equiv=Pragma content=no-cache><LINK 
href="Row Properties.files/dialog.css" type=text/css rel=stylesheet><LINK 
href="Row Properties.files/toolbar.css" type=text/css rel=stylesheet>
<SCRIPT language=javascript src="Row Properties.files/toolbar.js.php"></SCRIPT>

<SCRIPT language=javascript src="Row Properties.files/utils.js"></SCRIPT>

<SCRIPT language=javascript>
function callColorDlg()
{
	var sColor = dlgHelper.ChooseColorDlg();
	sColor = sColor.toString(16);	//change decimal to hex

	if (sColor.length < 6)	//add extra zeroes if hex number is less than 6 digits
	{
		var sTempString = "000000".substring(0,6-sColor.length);
		sColor = sTempString.concat(sColor);
	}
	return sColor;
}

function showColorPicker(curcolor)
{
	var newcol = callColorDlg();
	try
	{
		tr_prop.cbgcolor.value = newcol;
		tr_prop.color_sample.style.backgroundColor = tr_prop.cbgcolor.value;
	}
	catch (excp) {}
}

function Init()
{
	var cProps = window.dialogArguments;
	if (cProps)
	{
		tr_prop.cbgcolor.value = cProps.bgColor;
		tr_prop.color_sample.style.backgroundColor = tr_prop.cbgcolor.value;
		setHAlign(cProps.align);
		setVAlign(cProps.vAlign);
      
		if (cProps.styleOptions)
		{
			for (i=1; i<cProps.styleOptions.length; i++)
			{
				var oOption = document.createElement("OPTION");
				tr_prop.ccssclass.add(oOption);
				oOption.innerText = cProps.styleOptions[i].innerText;
				oOption.value = cProps.styleOptions[i].value;
  
				if (cProps.className) {tr_prop.ccssclass.value = cProps.className;}
			}
		}
	}
	resizeDialogToContent();
}

function okClick()
{
	var cprops = {};
	cprops.align = (tr_prop.chalign.value)?(tr_prop.chalign.value):'';
	cprops.vAlign = (tr_prop.cvalign.value)?(tr_prop.cvalign.value):'';
	cprops.bgColor = tr_prop.cbgcolor.value;
	cprops.className = (tr_prop.ccssclass.value != 'default')?tr_prop.ccssclass.value:'';

	window.returnValue = cprops;
	window.close();
}

function setSample()
{
	try {tr_prop.color_sample.style.backgroundColor = tr_prop.cbgcolor.value;}
	catch (excp) {}
}

function setHAlign(alignment)
{
	switch (alignment)
	{
		case "left":
			tr_prop.ha_left.className = "span_WXEDIT_default_tb_down";
			tr_prop.ha_center.className = "";
			tr_prop.ha_right.className = "";
			break;
		case "center":
			tr_prop.ha_left.className = "";
			tr_prop.ha_center.className = "span_WXEDIT_default_tb_down";
			tr_prop.ha_right.className = "";
			break;
		case "right":
			tr_prop.ha_left.className = "";
			tr_prop.ha_center.className = "";
			tr_prop.ha_right.className = "span_WXEDIT_default_tb_down";
			break;
	}
	tr_prop.chalign.value = alignment;
}

function setVAlign(alignment)
{
	switch (alignment)
	{
      	case "top":
		tr_prop.ha_top.className = "span_WXEDIT_default_tb_down";
		tr_prop.ha_middle.className = "";
		tr_prop.ha_bottom.className = "";
		tr_prop.ha_baseline.className = "";
		break;
	case "middle":
		tr_prop.ha_top.className = "";
		tr_prop.ha_middle.className = "span_WXEDIT_default_tb_down";
		tr_prop.ha_bottom.className = "";
		tr_prop.ha_baseline.className = "";
		break;
	case "bottom":
		tr_prop.ha_top.className = "";
		tr_prop.ha_middle.className = "";
		tr_prop.ha_bottom.className = "span_WXEDIT_default_tb_down";
		tr_prop.ha_baseline.className = "";
		break;
	case "baseline":
		tr_prop.ha_top.className = "";
		tr_prop.ha_middle.className = "";
		tr_prop.ha_bottom.className = "";
		tr_prop.ha_baseline.className = "span_WXEDIT_default_tb_down";
		break;
	}
	tr_prop.cvalign.value = alignment;
}
</SCRIPT>

<META content="MSHTML 6.00.2600.0" name=GENERATOR></HEAD>
<BODY onload=Init()>
<OBJECT id=dlgHelper height=0px width=0px 
classid=clsid:3050f819-98b5-11cf-bb82-00aa00bdce0b></OBJECT>
<FORM name=tr_prop>
<TABLE cellSpacing=0 cellPadding=2 width=336 border=0>
  <TBODY>
  <TR>
    <TD class=txt>Horizontal align:</TD>
    <TD align=right><INPUT type=hidden name=chalign></TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            class=dialog_WXEDIT_default_tb_out 
            onmousedown=dialog_WXEDIT_default_bt_down(this) id=ha_left 
            onmouseover=dialog_WXEDIT_default_bt_over(this) 
            onclick="setHAlign('left');" 
            onmouseout=dialog_WXEDIT_default_bt_out(this) alt=Left 
            src="Row Properties.files/tb_left.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            class=dialog_WXEDIT_default_tb_out 
            onmousedown=dialog_WXEDIT_default_bt_down(this) id=ha_center 
            onmouseover=dialog_WXEDIT_default_bt_over(this) 
            onclick="setHAlign('center');" 
            onmouseout=dialog_WXEDIT_default_bt_out(this) alt=Center 
            src="Row Properties.files/tb_center.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            class=dialog_WXEDIT_default_tb_out 
            onmousedown=dialog_WXEDIT_default_bt_down(this) id=ha_right 
            onmouseover=dialog_WXEDIT_default_bt_over(this) 
            onclick="setHAlign('right');" 
            onmouseout=dialog_WXEDIT_default_bt_out(this) alt=Right 
            src="Row Properties.files/tb_right.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD class=txt>Vertical align:</TD>
    <TD align=right><INPUT type=hidden name=cvalign></TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            class=dialog_WXEDIT_default_tb_out 
            onmousedown=dialog_WXEDIT_default_bt_down(this) id=ha_top 
            onmouseover=dialog_WXEDIT_default_bt_over(this) 
            onclick="setVAlign('top');" 
            onmouseout=dialog_WXEDIT_default_bt_out(this) alt=Top 
            src="Row Properties.files/tb_top.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            class=dialog_WXEDIT_default_tb_out 
            onmousedown=dialog_WXEDIT_default_bt_down(this) id=ha_middle 
            onmouseover=dialog_WXEDIT_default_bt_over(this) 
            onclick="setVAlign('middle');" 
            onmouseout=dialog_WXEDIT_default_bt_out(this) alt=Middle 
            src="Row Properties.files/tb_middle.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            class=dialog_WXEDIT_default_tb_out 
            onmousedown=dialog_WXEDIT_default_bt_down(this) id=ha_bottom 
            onmouseover=dialog_WXEDIT_default_bt_over(this) 
            onclick="setVAlign('bottom');" 
            onmouseout=dialog_WXEDIT_default_bt_out(this) alt=Bottom 
            src="Row Properties.files/tb_bottom.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            class=dialog_WXEDIT_default_tb_out 
            onmousedown=dialog_WXEDIT_default_bt_down(this) id=ha_baseline 
            onmouseover=dialog_WXEDIT_default_bt_over(this) 
            onclick="setVAlign('baseline');" 
            onmouseout=dialog_WXEDIT_default_bt_out(this) alt=Baseline 
            src="Row Properties.files/tb_baseline.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=2 border=0>
  <TBODY>
  <TR>
    <TD class=txt>CSS Class:</TD>
    <TD colSpan=3><SELECT class=input id=ccssclass size=1 
      name=ccssclass></SELECT></TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=2 border=0>
  <TBODY>
  <TR>
    <TD class=txt>Background color: <IMG id=color_sample height=16 
      src="Row Properties.files/spacer.gif" width=30 align=absBottom 
      border=1>&nbsp;<INPUT class=input_color onkeyup=setSample() size=7 
      name=cbgcolor maxlenght="7">&nbsp;</TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            class=WXEDIT_default_tb_out onmousedown=WXEDIT_default_bt_down(this) 
            id=colorpicker onmouseover=WXEDIT_default_bt_over(this) 
            onclick=showColorPicker(cbgcolor.value) 
            onmouseout=WXEDIT_default_bt_out(this) alt="Background color" 
            src="Row Properties.files/tb_colorpicker.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
<HR width="100%">

<TABLE cellSpacing=0 cellPadding=2 width="100%" border=0>
  <TBODY>
  <TR>
    <TD noWrap align=right colSpan=4><INPUT class=bt onfocus=if(this.blur)this.blur() onclick=okClick() type=button value="   OK   "> 
<INPUT class=bt onfocus=if(this.blur)this.blur() onclick=window.close() type=button value=Cancel> 
    </TD></TR></FORM></TBODY></TABLE></BODY></HTML>
