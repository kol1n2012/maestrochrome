function WXEDIT_default_bt_over(ctrl)
{
	ctrl.parentNode.className='span_WXEDIT_default_tb_over';
}
function WXEDIT_default_bt_out(ctrl)
{
	ctrl.parentNode.className='';
}
function WXEDIT_default_bt_down(ctrl)
{
	if (ctrl.parentNode.className != 'span_WXEDIT_default_tb_off')
	  ctrl.parentNode.className='span_WXEDIT_default_tb_down';
}
function WXEDIT_default_bt_up(ctrl)
{
	if (ctrl.parentNode.className != 'span_WXEDIT_default_tb_off')
	ctrl.parentNode.className='span_WXEDIT_default_tb_up';
}
function WXEDIT_default_bt_off(ctrl)
{
	ctrl.parentNode.className='span_WXEDIT_default_tb_off';
	ctrl.disabled = true;
}

function dialog_WXEDIT_default_bt_over(ctrl)
{
	if (ctrl.className != 'span_WXEDIT_default_tb_down')
	  ctrl.className='span_WXEDIT_default_tb_over';
}
function dialog_WXEDIT_default_bt_out(ctrl)
{
	if (ctrl.className == 'span_WXEDIT_default_tb_over')
	  ctrl.className='';
}
function dialog_WXEDIT_default_bt_down(ctrl)
{
	ctrl.className='span_WXEDIT_default_tb_down';
}
function dialog_WXEDIT_default_bt_off(ctrl)
{
	ctrl.className='span_WXEDIT_default_tb_off';
	ctrl.disabled = true;
}
