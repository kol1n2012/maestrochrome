function dialog_WXEDIT_base_image_name(ctrl)
{
	var imgname = ctrl.src.substring(0,ctrl.src.lastIndexOf("/"))+"/tb_"+ctrl.id.substr(ctrl.id.lastIndexOf("_tb_")+4, ctrl.id.length);
	return imgname;
}

function open_popup(link,windowname, w, h, form)
{
	var win=null;
	var t=null;
	var l=null;

	t=(screen.width-w)/2;
	l=(screen.height-h)/2;
	settings='width='+w+',height='+h+',top='+l+',left='+t+',scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
	win=window.open(link,windowname,settings);
	if (form!='') {form.target=windowname;}
}

function resizeDialogToContent()
{
	// resize window so there are no scrollbars visible
	var dw = window.dialogWidth;
	while (isNaN(dw))
	{
		dw = dw.substr(0,dw.length-1);
	}
	difw = dw - this.document.body.clientWidth;
	window.dialogWidth = this.document.body.scrollWidth+difw+'px';

	var dh = window.dialogHeight;
	while (isNaN(dh))
	{
		dh = dh.substr(0,dh.length-1);
	}
	difh = dh - this.document.body.clientHeight;
	window.dialogHeight = this.document.body.scrollHeight+difh+'px';
}
  
