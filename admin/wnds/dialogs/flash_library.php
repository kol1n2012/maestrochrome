
<?include("../../config.php");?>

<HTML><HEAD><TITLE>Insert a Flash object</TITLE>
<META http-equiv=Pragma content=no-cache>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
href="Insert a Flash object.files/dialog.css" type=text/css rel=stylesheet><LINK 
href="Insert a Flash object.files/toolbar.css" type=text/css rel=stylesheet>
<SCRIPT language=javascript 
src="Insert a Flash object.files/toolbar.js.php"></SCRIPT>

<SCRIPT language=javascript src="Insert a Flash object.files/utils.js"></SCRIPT>

<SCRIPT language=javascript>
function callColorDlg()
{
	var sColor = dlgHelper.ChooseColorDlg();
	sColor = sColor.toString(16);	//change decimal to hex

	if (sColor.length < 6)	//add extra zeroes if hex number is less than 6 digits
	{
		var sTempString = "000000".substring(0,6-sColor.length);
		sColor = sTempString.concat(sColor);
	}
	return sColor;
}

function showColorPicker(curcolor)
{
	var newcol = callColorDlg();
	try
	{
		flash_insert.cbgcolor.value = newcol;
		flash_insert.color_sample.style.backgroundColor = flash_insert.cbgcolor.value;
	}
	catch (excp) {}
}

function validateParams()
{
	// check width and height
	if (isNaN(parseInt(flash_insert.cwidth.value)) && flash_insert.cwidth.value != '')
	{
		alert('Error: Width is not a number');
		flash_insert.cwidth.focus();
		return false;
	}
	if (isNaN(parseInt(flash_insert.cheight.value)) && flash_insert.cheight.value != '')
	{
		alert('Error: Height is not a number');
		flash_insert.cheight.focus();
		return false;
	}
	return true;
}

function setSample()
{
	try {flash_insert.color_sample.style.backgroundColor = flash_insert.cbgcolor.value;}
	catch (excp) {}
}

function selectClick(path)
{
	if (validateParams())
	{
		var Rwidth = (flash_insert.cwidth.value)?(flash_insert.cwidth.value + flash_insert.cwunits.value):'';
		var Rheight = (flash_insert.cheight.value)?(flash_insert.cheight.value + flash_insert.chunits.value):'';
		var Rquality = (flash_insert.qualityflash.value)?flash_insert.qualityflash.value:'';
		var RbgColor = flash_insert.cbgcolor.value;
		window.opener.WXEDIT_flash_insert("Texte", path, Rwidth, Rheight, Rquality, RbgColor);
		self.close();
	}
	else {alert('Error: Please select a file');}
}
</SCRIPT>

<META content="MSHTML 6.00.2600.0" name=GENERATOR></HEAD>
<BODY onload=Init()>
<SCRIPT language=javascript>window.name = 'imglibrary';</SCRIPT>

<OBJECT id=dlgHelper height=0px width=0px 
classid=clsid:3050f819-98b5-11cf-bb82-00aa00bdce0b></OBJECT>
<DIV 
style="BORDER-RIGHT: black 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 1px solid; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: black 1px solid; PADDING-TOP: 5px; BORDER-BOTTOM: black 1px solid">
<FORM name=flash_insert action=flash_library.php method=post target=imglibrary 
encType=multipart/form-data><INPUT type=hidden value=default name=theme> <INPUT 
type=hidden value=en name=lang> 

<TABLE cellSpacing=0 cellPadding=2 width="95%" border=0>
  <TBODY>

<?php 

$OpenD = "../../../files/flash/";
echo $OpenD;
echo "<p>";
@mkdir($OpenD);
if ($handle = opendir($OpenD)) { 
    while (false !== ($file = readdir($handle))) {  
if (preg_match("/\.swf$/i",$file)) {  
    echo "
  <TR>
    <TD vAlign=top width=100%>
<table width=100% border=0>
<tr><TD style='FONT-SIZE: 10px'><IMG src='Insert a Flash object.files/swf.png' align=absMiddle>&nbsp;<B>$file</B></TD>
          <TD style='FONT-SIZE: 10px'>&nbsp;</TD>
          <TD style='PADDING-RIGHT: 10px' align=right><A 
           onclick=\"open_popup('".$siteurl."files/flash/$file','Preview','600','400','');\" 
            ><IMG 
            alt=View src='Insert a Flash object.files/view.png' align=absMiddle style='cursor: hand'
            border=0></A>&nbsp; <A 
            onclick=\"selectClick('".$siteurl."files/flash/$file');\" 
            ><IMG 
             style='cursor: hand' alt=Select src=\"Insert a Flash object.files/select.png\" 
            align=absMiddle border=0></A><BR></TD></TR></table>
      <HR>
    </TD></TR>
";  
}     } 
    closedir($handle); 
} 

?>

</TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=2 border=0>
  <TBODY>
  <TR>
    <TD>Width:&nbsp;</TD>
    <TD noWrap><INPUT class=input_small size=3 name=cwidth maxlenght="3"> 
      <SELECT class=input size=1 name=cwunits> <OPTION value=% 
        selected>%</OPTION> <OPTION value=px>px</OPTION></SELECT> </TD>
    <TD noWrap>&nbsp;&nbsp;Quality:&nbsp;</TD>
    <TD noWrap><SELECT class=input id=qualityflash size=1 name=qualityflash> 
        <OPTION value=High selected>High</OPTION> <OPTION 
      value=Low>Low</OPTION></SELECT> </TD></TR>
  <TR>
    <TD>Height:</TD>
    <TD noWrap><INPUT class=input_small size=3 name=cheight maxlenght="3"> 
      <SELECT class=input size=1 name=chunits> <OPTION value=% 
        selected>%</OPTION> <OPTION value=px>px</OPTION></SELECT> </TD></TR>
  <TR>
    <TD>Background Color:</TD>
    <TD><IMG id=color_sample height=15 
      src="Insert a Flash object.files/spacer.gif" width=30 align=absMiddle 
      border=1>&nbsp;<INPUT class=input_color onkeyup=setSample() size=7 
      name=cbgcolor maxlenght="7">&nbsp;</TD>
    <TD>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            onmouseup=WXEDIT_default_bt_up(this) class=WXEDIT_default_tb_out 
            onmousedown=WXEDIT_default_bt_down(this) id=colorpicker 
            onmouseover=WXEDIT_default_bt_over(this) 
            onclick=showColorPicker(cbgcolor.value) 
            onmouseout=WXEDIT_default_bt_out(this) alt="Background Color" 
            src="Insert a Flash object.files/tb_colorpicker.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left colSpan=3><INPUT class=bt onclick=window.close(); type=button value=Cancel></TD></TR></TBODY></TABLE></DIV>
<DIV 
style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px; TEXT-ALIGN: center">
<HR style="WIDTH: 200px">
</DIV>
</form><form action="../file_upload.php" method="post" enctype='multipart/form-data' >
<DIV 
style="BORDER-RIGHT: black 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 1px solid; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: black 1px solid; PADDING-TOP: 5px; BORDER-BOTTOM: black 1px solid">
<TABLE cellSpacing=0 cellPadding=2 border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left><B>Upload a Flash file:
    <input type=hidden name=name value="../files/flash">
</B></TD>
    <TD><INPUT class=input type=file name=newfile></TD>
    <TD><INPUT class=bt type=submit value=Upload name=btnupload></TD></TR></TBODY></TABLE></DIV></FORM></BODY></HTML>
