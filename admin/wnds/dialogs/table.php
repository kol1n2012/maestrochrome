<HTML><HEAD><TITLE>Table Properties</TITLE>
<META http-equiv=Pragma content=no-cache>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
href="Table Properties1.files/dialog.css" type=text/css rel=stylesheet><LINK 
href="Table Properties1.files/toolbar.css" type=text/css rel=stylesheet>
<SCRIPT language=javascript 
src="Table Properties1.files/toolbar.js.php"></SCRIPT>

<SCRIPT language=javascript src="Table Properties1.files/utils.js"></SCRIPT>

<SCRIPT language=javascript>
function callColorDlg()
{
	var sColor = dlgHelper.ChooseColorDlg();
	sColor = sColor.toString(16);	//change decimal to hex

	if (sColor.length < 6)	//add extra zeroes if hex number is less than 6 digits
	{
		var sTempString = "000000".substring(0,6-sColor.length);
		sColor = sTempString.concat(sColor);
	}
	return sColor;
}
function showColorPicker(curcolor)
{
	var newcol = callColorDlg();
	try {
		table_prop.tbgcolor.value = newcol;
		table_prop.color_sample.style.backgroundColor = table_prop.tbgcolor.value;
	}
 	catch (excp) {}
}

function Init()
{
    var tProps = window.dialogArguments;
    if (tProps)
    {
		// set attribute values
		table_prop.trows.value = '3';
		table_prop.trows.disabled = true;
		table_prop.tcols.value = '3';
		table_prop.tcols.disabled = true;

		table_prop.tborder.value = tProps.border;
		table_prop.tcpad.value = tProps.cellPadding;
		table_prop.tcspc.value = tProps.cellSpacing;
		table_prop.tbgcolor.value = tProps.bgColor;
		table_prop.color_sample.style.backgroundColor = table_prop.tbgcolor.value;
		if (tProps.width)
		{
			if (!isNaN(tProps.width) || (tProps.width.substr(tProps.width.length-2,2).toLowerCase() == "px"))
			{
				// pixels
				if (!isNaN(tProps.width))
				  table_prop.twidth.value = tProps.width;
				else
				  table_prop.twidth.value = tProps.width.substr(0,tProps.width.length-2);
				table_prop.twunits.options[0].selected = false;
				table_prop.twunits.options[1].selected = true;
			}
			else
			{
				// percents
				table_prop.twidth.value = tProps.width.substr(0,tProps.width.length-1);
				table_prop.twunits.options[0].selected = true;
				table_prop.twunits.options[1].selected = false;
			}
		}
		if (tProps.width)
		{
			if (!isNaN(tProps.height) || (tProps.height.substr(tProps.height.length-2,2).toLowerCase() == "px"))
			{
				// pixels
				if (!isNaN(tProps.height))
				  table_prop.theight.value = tProps.height;
				else
				  table_prop.theight.value = tProps.height.substr(0,tProps.height.length-2);
				table_prop.thunits.options[0].selected = false;
				table_prop.thunits.options[1].selected = true;
			}
			else
			{
				// percents
				table_prop.theight.value = tProps.height.substr(0,tProps.height.length-1);
				table_prop.thunits.options[0].selected = true;
				table_prop.thunits.options[1].selected = false;
			}
		}
	}
	else
    {
		// set default values
		table_prop.trows.value = '3';
		table_prop.tcols.value = '3';
		table_prop.tborder.value = '0';
	}
	resizeDialogToContent();
}

function validateParams()
{
	// check whether rows and cols are integers
	if (isNaN(parseInt(table_prop.trows.value)))
	{
		alert('Error: Rows is not a number');
		table_prop.trows.focus();
		return false;
	}
	if (isNaN(parseInt(table_prop.tcols.value)))
	{
		alert('Error: Columns is not a number');
		table_prop.tcols.focus();
		return false;
	}

	// check width and height
    if (isNaN(parseInt(table_prop.twidth.value)) && table_prop.twidth.value != '')
    {
      alert('Error: Width is not a number');
      table_prop.twidth.focus();
      return false;
    }
	if (isNaN(parseInt(table_prop.theight.value)) && table_prop.theight.value != '')
	{
		alert('Error: Height is not a number');
		table_prop.theight.focus();
		return false;
	}

    // check border, padding and spacing
    if (isNaN(parseInt(table_prop.tborder.value)) && table_prop.tborder.value != '')
	{
		alert('Error: Border is not a number');
		table_prop.tborder.focus();
		return false;
	}
	if (isNaN(parseInt(table_prop.tcpad.value)) && table_prop.tcpad.value != '')
	{
		alert('Error: Cell padding is not a number');
		table_prop.tcpad.focus();
		return false;
	}
	if (isNaN(parseInt(table_prop.tcspc.value)) && table_prop.tcspc.value != '')
	{
		alert('Error: Cell spacing is not a number');
		table_prop.tcspc.focus();
		return false;
	}
	return true;
}

function okClick()
{
    if (validateParams())	// validate paramters
	{
		var newtable = {};
		newtable.width = (table_prop.twidth.value)?(table_prop.twidth.value + table_prop.twunits.value):'';
		newtable.height = (table_prop.theight.value)?(table_prop.theight.value + table_prop.thunits.value):'';
		newtable.border = table_prop.tborder.value;
		newtable.cols = table_prop.tcols.value;
		newtable.rows = table_prop.trows.value
		newtable.cellPadding = table_prop.tcpad.value;
		newtable.cellSpacing = table_prop.tcspc.value;
		newtable.bgColor = table_prop.tbgcolor.value;

		window.returnValue = newtable;
		window.close();
	}
}

function setSample()
{
	try {table_prop.color_sample.style.backgroundColor = table_prop.tbgcolor.value;}
	catch (excp) {}
}
</SCRIPT>

<META content="MSHTML 6.00.2600.0" name=GENERATOR></HEAD>
<BODY onload=Init()>
<OBJECT id=dlgHelper height=0px width=0px 
classid=clsid:3050f819-98b5-11cf-bb82-00aa00bdce0b></OBJECT>
<TABLE cellSpacing=0 cellPadding=2 width=336 border=0>
  <FORM name=table_prop>
  <TBODY>
  <TR>
    <TD class=txt>Rows:</TD>
    <TD><INPUT class=input_small size=3 name=trows maxlenght="3"></TD>
    <TD class=txt>Columns:</TD>
    <TD><INPUT class=input_small size=3 name=tcols maxlenght="3"></TD></TR>
  <TR>
    <TD class=txt>Width:</TD>
    <TD noWrap><INPUT class=input_small size=3 name=twidth maxlenght="3"> 
      <SELECT class=input_small size=1 name=twunits> <OPTION value=% 
        selected>%</OPTION> <OPTION value=px>px</OPTION></SELECT> </TD>
    <TD class=txt>Height:</TD>
    <TD noWrap><INPUT class=input_small size=3 name=theight maxlenght="3"> 
      <SELECT class=input_small size=1 name=thunits> <OPTION value=% 
        selected>%</OPTION> <OPTION value=px>px</OPTION></SELECT> </TD></TR>
  <TR>
    <TD class=txt>Border:</TD>
    <TD colSpan=3><INPUT class=input_small size=2 name=tborder maxlenght="2"> 
      pixels</TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=2 border=0>
  <TBODY>
  <TR>
    <TD class=txt>Cell padding:</TD>
    <TD><INPUT class=input_small size=3 name=tcpad maxlenght="3"></TD></TR>
  <TR>
    <TD class=txt>Cell spacing:</TD>
    <TD><INPUT class=input_small size=3 name=tcspc 
maxlenght="3"></TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=2 border=0>
  <TBODY>
  <TR>
    <TD class=txt>Background color: <IMG id=color_sample height=16 
      src="Table Properties1.files/spacer.gif" width=30 align=absBottom 
      border=1>&nbsp;<INPUT class=input_color onkeyup=setSample() size=7 
      name=tbgcolor maxlenght="7">&nbsp;</TD>
    <TD align=left>
      <TABLE height=25 cellSpacing=0 cellPadding=0 width=25 border=0>
        <TBODY>
        <TR>
          <TD class=span_WXEDIT_default_tb vAlign=center align=middle><IMG 
            onmouseup=WXEDIT_default_bt_up(this) class=WXEDIT_default_tb_out 
            onmousedown=WXEDIT_default_bt_down(this) id=colorpicker 
            onmouseover=WXEDIT_default_bt_over(this) 
            onclick=showColorPicker(tbgcolor.value) 
            onmouseout=WXEDIT_default_bt_out(this) alt="Background color" 
            src="Table Properties1.files/tb_colorpicker.gif" align=absBottom 
            unselectable="on"></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=2 width="100%" border=0>
  <TBODY>
  <TR>
    <TD noWrap colSpan=2>
      <HR width="100%">
    </TD></TR>
  <TR>
    <TD noWrap align=right colSpan=2><INPUT class=bt onfocus=if(this.blur)this.blur() onclick=okClick() type=button value="   OK   "> 
<INPUT class=bt onfocus=if(this.blur)this.blur() onclick=window.close() type=button value=Cancel> 
    </TD></TR></FORM></TBODY></TABLE></BODY></HTML>
