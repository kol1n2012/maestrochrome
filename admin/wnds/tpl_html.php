<?

function html_modify($fname) {
  $fp=fopen($fname,r);
  $s=@fread($fp,filesize($fname));
  fclose($fp);

  $s=change_urls($s);

//  $fp=fopen($fname,w);
//  fwrite($fp,$s);
//  fclose($fp);

  return $s;
}

function change_urls($s) {
    change_for("a","href",$s);
    change_for("link","href",$s);
    change_for("img","src",$s);
    change_for("td","background",$s);
    change_for("body","background",$s);
    change_for("input","src",$s);
    return $s;
}

function change_for($tag,$link,&$s) {
    global $pref;

    $ltag=strlen($tag)+1;
    $llink=strlen($link)+1;

    $ls=strtolower($s);
    $n=strpos($ls,"<".$tag);
    while ($n!==false) {
	$zn=strpos($ls,$link."=",$n+$ltag);
	if ($zn!==false) {
	    $par=substr($ls,$zn+$llink,1);
	    if ($par!='"' && $par!="'") {
		$zn+=$llink;
		$ezn=strpos($ls," ",$zn);
		$ezn1=strpos($ls,">",$zn);
		if ($ezn1<$ezn) $ezn=$ezn1;
		$path=substr($s,$zn,$ezn-$zn);
	    } else {
		$zn+=$llink+1;
	        $ezn=strpos($ls,$par,$zn);
	        $path=substr($s,$zn,$ezn-$zn);
	    }

	    if ($path[0]!='{' && $path[0]!='#' && strpos(strtolower($path),"mailto:")===false && strpos(strtolower($path),"javascript:")===false && strpos($path,"://")===false && substr($path,0,1)!="/") {
	        $path=$pref."/".$path;
	        $s=substr($s,0,$zn).$path.substr($s,$ezn);
		$ls=substr($ls,0,$zn).$path.substr($ls,$ezn);
		$ezn+=strlen($pref)+1;
	    }
	
    	    $n=strpos($ls,"<".$tag,$ezn);
	} else 
    	    $n=strpos($ls,"<".$tag,$n+$ltag);
    }
}

?>