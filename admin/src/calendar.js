function CheckDate() {
  if (!document.getElementById('euse')) return true;
  if (!document.getElementById('euse').checked) return true;

  var tyear=document.getElementById('year').value;
  var tmonth=document.getElementById('month').value;
  var tday=document.getElementById('day').value;
  var thour=document.getElementById('hour').value;
  var tmin=document.getElementById('min').value;
  var eyear=document.getElementById('eyear').value;
  var emonth=document.getElementById('emonth').value;
  var eday=document.getElementById('eday').value;
  var ehour=document.getElementById('ehour').value;
  var emin=document.getElementById('emin').value;

  var aok=false;
  if (tyear<eyear) aok=true;
   else if (tyear==eyear) {
     if (tmonth<emonth) aok=true;
      else if (tmonth==emonth) {
        if (tday<eday) aok=true;
         else if (tday==eday) {
           if (thour<ehour) aok=true;
            else if (thour==ehour) {
             if (tmin<=emin) aok=true;
              else aok=false;
            } else aok=false;
         } else aok=false;
      } else aok=false;
   } else aok=false;

  if (!aok) alert('��������! �� ����� �������� ���� ��������� ����������.');

  return aok;
}
function CalendShow(mode) {
  if (cshowed==true) {
    CalendHide();
    if (cmode==mode) return;
  }
  cmode=mode;
  el=document.getElementById('calend1');
  if (mode>2) {
    el.style.position='relative';
    el.style.left=256*(mode-3)+'px';
    el.style.top='46px';
  } else {
    el.style.position='absolute';
    el.style.left=30+256*(mode-1)+'px';
    el.style.top='136px';
  }
  el.style.display='block';
  cshowed=true;
  if ((mode-1)%2+1==2) pr='e'; else pr='';

  tyear=document.getElementById(pr+'year').value;
  tmonth=document.getElementById(pr+'month').value;
  tday=document.getElementById(pr+'day').value;
  thour=document.getElementById(pr+'hour').value;
  tmin=document.getElementById(pr+'min').value;

  date1=new Date(tyear,tmonth-1,1);
  day=date1.getDay();
  if (day==0) day=7;

  if (tmonth==1) { dmon=31;ms='������'; }
  if (tmonth==2) {
    if (tyear%4==0) dmon=29; else dmon=28;
    ms='�������';  }
  if (tmonth==3) { dmon=31;ms='����'; }
  if (tmonth==4) { dmon=30;ms='������'; }
  if (tmonth==5) { dmon=31;ms='���'; }
  if (tmonth==6) { dmon=30;ms='����'; }
  if (tmonth==7) { dmon=31;ms='����'; }
  if (tmonth==8) { dmon=31;ms='������'; }
  if (tmonth==9) { dmon=30;ms='��������'; }
  if (tmonth==10) { dmon=31;ms='�������'; }
  if (tmonth==11) { dmon=30;ms='������'; }
  if (tmonth==12) { dmon=31;ms='�������'; }

  document.getElementById('jmon').innerHTML=ms;
  document.getElementById('jyear').innerHTML=tyear;
  document.getElementById('sh').innerHTML=thour;
  document.getElementById('sm').innerHTML=tmin;

  for (i=1;i<7;i++) {
    for (j=1;j<8;j++) {
      s=' ';
      if (i==1 && day<=j) s=j-day+1;
       else if (i>1) {
        s=(i-1)*7+j-day+1;
        if (s>dmon) s=' ';
      }
      el=document.getElementById('s'+i+j);
      el.innerHTML=s;
      if (s==tday) el.style.background='#FFE0D0';
       else el.style.background='none';
    }
  }
}
function clClick(i,j) {
  el=document.getElementById('s'+i+j);
  s=el.innerHTML;
  if (s=='') return false;
  s=s<10?'0'+s:s;
  document.getElementById(pr+'day').value=s;
  if ((cmode-1)%2+1==2) CalendHide2();
   else CalendHide();
}
function shClick() {
  el=document.getElementById('sh');
  s=el.innerHTML;
  s++;
  if (s>23) s=0;
  s=s<10?'0'+s:s;
  document.getElementById(pr+'hour').value=s;
  el.innerHTML=s;
}
function smClick() {
  el2=document.getElementById('sm');
  s=el2.innerHTML;
  s++;
  if (s>59) {
   shClick();
   s=0;
  }
  s=s<10?'0'+s:s;
  document.getElementById(pr+'min').value=s;
  el2.innerHTML=s;
}
function PredYear() {
  el=document.getElementById(pr+'year');
  if (el.value>1970) el.value--;
  cshowed=false;
  CalendShow(cmode);
}
function NextYear() {
  el=document.getElementById(pr+'year');
  if (el.value<2050) el.value++;
  cshowed=false;
  CalendShow(cmode);
}
function PredMonth() {
  el=document.getElementById(pr+'month');
  if (el.value>1) {
    s=el.value;
    s--;
    s=s<10?'0'+s:s;
    el.value=s;
  } else {
    el.value=12;
    PredYear();
  }
  cshowed=false;
  CalendShow(cmode);
}
function NextMonth() {
  el=document.getElementById(pr+'month');
  if (el.value<12) {
    s=el.value;
    s++;
    s=s<10?'0'+s:s;
    el.value=s;
  } else {
    el.value='01';
    NextYear();
  }
  cshowed=false;
  CalendShow(cmode);
}

function CalendHide() {
  document.getElementById('calend1').style.display='none';
  cshowed=false;
}

function CalendHide2() {
  document.getElementById('euse').checked=true;
  document.getElementById('calend1').style.display='none';
  cshowed=false;
}
