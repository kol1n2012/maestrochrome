
function TplCreate(cur) {
   window.open('wnds/tpl_create.php?name='+cur,'tpl_create','resizable=no,menubar=no,scrollbars=no,width=400,height=160,top=1');
   return false;
}

function TplView(cur) {
   window.open('../tpl_view.php?name='+cur,'tpl_view','resizable=yes,menubar=yes,scrollbars=yes,width=800,height=600,top=1');
   return false;
}

function TplHTML(cur) {
   window.open('wnds/tpl_edit.php?name='+cur,'tpl_edit','resizable=no,menubar=no,scrollbars=no,width=700,height=600,top=1');
   return false;
}

function TplCSS(cur) {
   window.open('wnds/tpl_edit_css.php?name='+cur,'tpl_edit_css','resizable=no,menubar=no,scrollbars=no,width=700,height=600,top=1');
   return false;
}

function TplProps(cur) {
   window.open('wnds/tpl_props.php?name='+cur,'tpl_props','resizable=no,menubar=no,scrollbars=no,width=500,height=310,top=1');
   return false;
}

function TplDelete(cur) {
   window.open('wnds/tpl_delete.php?name='+cur,'tpl_delete','resizable=no,menubar=no,scrollbars=no,width=400,height=150,top=1');
   return false;
}

function TplHelp(cur) {
   window.open('wnds/tpl_help.php?name='+cur,'tpl_help','resizable=yes,menubar=no,scrollbars=yes,width=500,height=500,top=1');
   return false;
}


function MTplCreate(cur) {
   window.open('wnds/mtpl_create.php','mtpl_create','resizable=no,menubar=no,scrollbars=no,width=400,height=455,top=1');
   return false;
}

function MTplProps(cur) {
   window.open('wnds/mtpl_props.php?id='+cur,'mtpl_props','resizable=no,menubar=no,scrollbars=no,width=400,height=455,top=1');
   return false;
}

function MTplDelete(cur) {
   window.open('wnds/mtpl_delete.php?id='+cur,'mtpl_delete','resizable=no,menubar=no,scrollbars=no,width=400,height=150,top=1');
   return false;
}


function NTplCreate(cur) {
   window.open('wnds/ntpl_create.php','ntpl_create','resizable=no,menubar=no,scrollbars=no,width=400,height=455,top=1');
   return false;
}

function NTplProps(cur) {
   window.open('wnds/ntpl_props.php?id='+cur,'ntpl_props','resizable=no,menubar=no,scrollbars=no,width=400,height=500,top=1');
   return false;
}

function NTplDelete(cur) {
   window.open('wnds/ntpl_delete.php?id='+cur,'ntpl_delete','resizable=no,menubar=no,scrollbars=no,width=400,height=150,top=1');
   return false;
}


function NCTplCreate(cur) {
   window.open('wnds/nctpl_create.php','nctpl_create','resizable=no,menubar=no,scrollbars=no,width=400,height=455,top=1');
   return false;
}

function NCTplProps(cur) {
   window.open('wnds/nctpl_props.php?id='+cur,'nctpl_props','resizable=no,menubar=no,scrollbars=no,width=400,height=455,top=1');
   return false;
}

function NCTplDelete(cur) {
   window.open('wnds/nctpl_delete.php?id='+cur,'nctpl_delete','resizable=no,menubar=no,scrollbars=no,width=400,height=150,top=1');
   return false;
}


function NCPTplCreate(cur) {
   window.open('wnds/ncptpl_create.php','ncptpl_create','resizable=no,menubar=no,scrollbars=no,width=400,height=320,top=1');
   return false;
}

function NCPTplProps(cur) {
   window.open('wnds/ncptpl_props.php?id='+cur,'ncptpl_props','resizable=no,menubar=no,scrollbars=no,width=400,height=320,top=1');
   return false;
}

function NCPTplDelete(cur) {
   window.open('wnds/ncptpl_delete.php?id='+cur,'ncptpl_delete','resizable=no,menubar=no,scrollbars=no,width=400,height=150,top=1');
   return false;
}


function PTplCreate(cur) {
   window.open('wnds/ptpl_create.php','ptpl_create','resizable=no,menubar=no,scrollbars=no,width=400,height=270,top=1');
   return false;
}

function PTplProps(cur) {
   window.open('wnds/ptpl_props.php?id='+cur,'ptpl_props','resizable=no,menubar=no,scrollbars=no,width=400,height=270,top=1');
   return false;
}

function PTplDelete(cur) {
   window.open('wnds/ptpl_delete.php?id='+cur,'ptpl_delete','resizable=no,menubar=no,scrollbars=no,width=400,height=150,top=1');
   return false;
}