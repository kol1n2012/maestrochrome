
function CatCreate(cur) {
   window.open('wnds/cat_create.php?id='+cur,'cat_create','resizable=no,menubar=no,scrollbars=no,width=400,height=490,top=1');
   return false;
}

function CatProps(cur) {
   window.open('wnds/cat_props.php?id='+cur,'cat_props','resizable=no,menubar=no,scrollbars=no,width=400,height=490,top=1');
   return false;
}

function CatDelete(cur) {
   window.open('wnds/cat_delete.php?id='+cur,'cat_delete','resizable=no,menubar=no,scrollbars=no,width=400,height=160,top=1');
   return false;
}


function SubCatCreate(cur) {
   window.open('wnds/cat_create.php?sub=yes&id='+cur,'cat_create','resizable=no,menubar=no,scrollbars=no,width=400,height=160,top=1');
   return false;
}


function SubCatProps(cur) {
   window.open('wnds/cat_props.php?sub=yes&id='+cur,'cat_props','resizable=no,menubar=no,scrollbars=yes,width=600,height=600,top=1');
   return false;
}


function NewsProps(cur) {
   window.open('wnds/news_props.php?id='+cur,'news_props','resizable=no,menubar=no,scrollbars=no,width=534,height=580,top=1');
   return false;
}


function NewsHide(cur) {
   window.open('wnds/news_hide.php?id='+cur,'news_hide','resizable=no,menubar=no,scrollbars=no,width=10,height=10,top=1');
   return false;
}

function NewsActs(cur) {
   window.open('wnds/news_acts.php'+cur,'news_acts','resizable=no,menubar=no,scrollbars=no,width=600,height=150,top=1');
   return false;
}

function NewsSort(cur) {
   window.open('wnds/news_sort.php'+cur,'news_sort','resizable=no,menubar=no,scrollbars=no,width=10,height=10,top=1');
   return false;
}

function NewsToArch(cur) {
   window.open('wnds/news_arch.php?id='+cur,'news_arch','resizable=no,menubar=no,scrollbars=no,width=10,height=10,top=1');
   return false;
}

function NewsDelete(cur) {
   window.open('wnds/news_delete.php?id='+cur,'news_delete','resizable=no,menubar=no,scrollbars=no,width=400,height=150,top=1');
   return false;
}