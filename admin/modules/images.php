<?
function resize_img($fname,$dname,$x,$y) 
{
	list($w, $h, $type) = getimagesize($fname);    
	
	$old = false;
	switch($type)
	{
		case 1:{$old = imageCreateFromGif($fname);break;}
		case 2:{$old = imageCreateFromJpeg($fname);break;}
		case 3:{$old = imageCreateFromPng($fname);break;}
	}
    if ($old===false) return false;

    $s1=$w/$h;
    $y=round($x/$s1);

    $new = imageCreateTrueColor($x, $y);
	//$new = imageCreateTrueColor($x, $y);
 
    imageCopyResampled($new, $old, 0, 0, 0, 0, $x, $y, $w, $h);
    imageDestroy($old);

    imageJpeg($new, $dname);
    imageDestroy($new);

    return true;
}

//resize_img("11.jpg", "11.jpg", 50,20);

function makeImages($img_name, $small_img_name,$path)
{
	list($w, $h) = getimagesize($path.$img_name);
	if($w>157 || $h>98) resize_img($path.$img_name, $path.$small_img_name, 157,98);//Thumbnail
	else copy($path.$img_name, $path.$small_img_name);
	/*$maxW = 500;
	$maxH = 500;
	list($w, $h) = getimagesize($img_name);
	
	if($w<$maxW && $h<$maxH)
	{
		resize_img("1.jpg", "11.jpg", 500,200);
	}*/
}
?>