<?
  switch ($action) {
    case "pages":
        include "src/pages.php";
        break;
    case "news":
        include "src/news.php";
        break;
    case "gbook":
        include "src/gbook.php";
        break;
    case "files":
        include "src/files.php";
        break;
    case "design":
        include "src/design.php";
        break;
    case "props":
	include "src/props.php";
        break;
    case "sites":
	include "src/sites.php";
        break;
    case "admin":
	include "src/admin.php";
        break;
    case "banners":
	include "src/banners.php";
        break;
    case "stats":
	include "src/stats.php";
        break;
    	
    default:
        include "src/default.php";
  }
?>